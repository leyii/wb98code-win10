

# Win10五笔98字库

#### 介绍
将Win10的微软五笔86版字库替换为98版字库  

这是文章的[原地址](https://www.pianshen.com/article/8966889293/)  
这是[Github仓库](https://github.com/Karl1123/98wubi)的原地址  

为了防止原文章失效，上传了一个Word版的操作说明。